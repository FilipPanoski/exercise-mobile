import React, { Component } from 'react';
import {Router, Scene} from "react-native-router-flux";
import MainPage from './src/pages/main-page/MainPage';
import {connect, Provider} from "react-redux";
import store from "./src/store/store";

const RouterWithRedux = connect() (Router);

export default class App extends Component {

  render() {

    return (
        <Provider store={store}>
            <RouterWithRedux>
                <Scene key="root">
                    <Scene key="main" component={MainPage} title="Main Page" initial={true}/>
                </Scene>
            </RouterWithRedux>
        </Provider>
    );
  }
}
