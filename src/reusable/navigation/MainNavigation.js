import React from 'react';
import {RIGHTS_VIEW, ROLES_VIEW, USERS_VIEW} from "../../reducers/viewReducer";
import {Text, View} from "react-native";
import MenuItem from "./MenuItem";

const MainNavigation = (props) => {

    let handleItemClick = (view) => {
        props.changeView(view);
    };

    const view = props.view;

    return (
        <View>
            <Text>Exercise</Text>
            <View>
                <MenuItem
                    heading="Users"
                    view={USERS_VIEW}
                    onChangeView={handleItemClick}
                />
            </View>

            <View>
                <MenuItem
                    heading="Roles"
                    view={ROLES_VIEW}
                    onChangeView={handleItemClick}
                />
            </View>

            <View>
                <MenuItem
                    heading="Rights"
                    view={RIGHTS_VIEW}
                    onChangeView={handleItemClick}
                />
            </View>
        </View>
    );
};
export default MainNavigation;