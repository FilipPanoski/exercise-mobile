import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";

const MenuItem = (props) => {

    return (
        <View>
            <TouchableOpacity onPress={props.onChangeView(props.view)}>
                <Text name={props.view}>
                    {props.heading}
                </Text>
            </TouchableOpacity>
        </View>
    );
};
export default MenuItem;