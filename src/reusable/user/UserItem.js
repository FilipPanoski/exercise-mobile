import React from 'react';
import {View} from "react-native";

const UserItem = (props) => {

    return (
        <View className="data-item clickable">
            <h3>{props.user.username}</h3>
        </View>
    )
};
export default UserItem;