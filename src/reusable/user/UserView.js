import React, { Component } from 'react';
import {Text, View} from "react-native";
import {connect} from "react-redux";
import {checkIfArrayContains} from "../../util/DataUtility";
import UserItem from "./UserItem";
import {addUser, fetchUsers} from "../../actions/userActions";
import {addRoleToUser, fetchRoles} from "../../actions/roleActions";

class UserView extends Component {

    state = {
        users: [],
    };

    componentDidMount() {
        this.props.onFetchUsers();
        this.props.onFetchRoles();
    }

    componentWillReceiveProps(newProps) {
        let users = newProps.users;
        let user = newProps.user;
        if (user.hasOwnProperty('username') && !checkIfArrayContains(users, user)) {
            users.push(user);
        }

        this.setState({
            users: newProps.users,
        })
    }

    render() {
        let userItems = [];
        const users = this.state.users;
        const error = this.props.error;

        if(users.length) {
            userItems = users.map(user => (
                <View key={user.id}>
                    <UserItem user={user}/>
                </View>
            ));
        }

        return (
            <View>
                <Text>Users</Text>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    users: state.users.users,
    user: state.users.user,
    roles: state.roles.roles,
    error: state.users.error
});

const mapActionsToProps = {
    onFetchUsers: fetchUsers,
    onAddUser: addUser,
    onFetchRoles: fetchRoles,
    onAddRoleToUser: addRoleToUser
};
export default connect(mapStateToProps, mapActionsToProps)(UserView);