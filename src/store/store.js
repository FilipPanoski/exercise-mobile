import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/rootReducer';

const initialState = {};

const storeEnhancers = compose(
    applyMiddleware(thunk),
);

const store = createStore(
    rootReducer,
    initialState,
    storeEnhancers
);
export default store;