import {FETCH_RIGHTS, RIGHTS_ERROR} from "../reducers/rightReducer";
import Request from "superagent";
import {INTERNAL_SERVER_ERROR} from "../global/globalConfig";

export const fetchRights = () => dispatch => {
    Request.get('/rights').then(response => dispatch({
        type: FETCH_RIGHTS,
        payload: response.body
    })).catch(error => dispatch(errorHandler(error.response)) )
};

let errorHandler = (error) => {
    switch (error.statusCode) {
        case INTERNAL_SERVER_ERROR:
            return {
                type: RIGHTS_ERROR,
                payload: "Something went wrong."
            };
        default:
            return {
                type: RIGHTS_ERROR,
                payload: "Something went wrong."
            };
    }
};