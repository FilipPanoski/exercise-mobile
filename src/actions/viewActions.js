import { CHANGE_VIEW } from "../reducers/viewReducer";

export const changeView = (newView) => dispatch => {
    dispatch({
        type: CHANGE_VIEW,
        payload: newView
    })
};