import {
    ADD_ROLE_TO_USER, BAD_ROLE_ERROR, FETCH_ROLE, FETCH_ROLES, FETCH_USER_ROLES,
    REMOVE_RIGHT_FROM_ROLE, ROLES_ERROR
} from "../reducers/roleReducer";
import Request from "superagent";
import {BAD_REQUEST, INTERNAL_SERVER_ERROR, NOT_FOUND} from "../global/globalConfig";

export const fetchRoles = () => dispatch => {
    Request.get('/roles').then(response => {
        dispatch({
            type: FETCH_ROLES,
            payload: response.body
        })
    }).catch(error => dispatch(errorHandler(error.response)))

};

export const fetchUserRoles = (userId) => dispatch => {
    Request.get('/users/' + userId + '/roles').then(response => dispatch({
        type: FETCH_USER_ROLES,
        payload: response.body
    })).catch(error => dispatch(errorHandler(error.response)))
};

export const fetchRole = (roleId) => dispatch => {
    Request.get('/roles/' + roleId).then(response => dispatch({
        type: FETCH_ROLE,
        payload: response.body
    })).catch(error => dispatch(errorHandler(error.response)))
};

export const addRoleToUser = (userId, roleId) => dispatch => {
    Request.put('/add-role/users/' + userId + '/roles/' + roleId).then(response => dispatch({
        type: ADD_ROLE_TO_USER,
        payload: response.body
    })).catch(error => dispatch(errorHandler(error.response)))
};

export const removeRightFromRole = (rightId, roleId) => dispatch => {
    Request.put('/remove-right/rights/' + rightId + '/roles/' + roleId).then(response => dispatch({
        type: REMOVE_RIGHT_FROM_ROLE,
        payload: response.body
    })).catch(error => dispatch(errorHandler(error.response)))
};

let errorHandler = (error) => {
    switch (error.statusCode) {
        case INTERNAL_SERVER_ERROR:
            return {
                type: BAD_ROLE_ERROR,
                payload: "Something went wrong."
            };
        case BAD_REQUEST:
            return {
                type: BAD_ROLE_ERROR,
                payload: "User already has that role."
            };
        case NOT_FOUND:
            return {
                type: ROLES_ERROR,
                payload: "No roles were found."
            };
        default:
            return {
                type: BAD_ROLE_ERROR,
                payload: "Something went wrong."
            };
    }
};