import {FETCH_USERS, ADD_USER, USERS_ERROR, FETCH_USER } from "../reducers/userReducer";
import Request from "superagent";
import {BAD_REQUEST, INTERNAL_SERVER_ERROR, NOT_FOUND} from "../global/globalConfig";

export const fetchUsers = () => dispatch => {
    Request.get('/users').then(response => {
        dispatch({
            type: FETCH_USERS,
            payload: response.body
        })
    }).catch(error => dispatch(errorHandler(error.response)))
};

export const fetchUser = (id) => dispatch => {
    Request.get('/users/' + id).then(response => {
        dispatch({
            type: FETCH_USER,
            payload: response.body
        })
    }).catch(error => dispatch(errorHandler(error.response)))
};

export const addUser = (user) => dispatch => {
    Request.post('/users', {
        username: user.username,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        countryCode: user.countryCode,
        mdbid: user.MDBID,
        gender: user.gender
    }).then(response => dispatch({
        type: ADD_USER,
        payload: response.body
    })).catch(error => dispatch(errorHandler(error.response)))
};

let errorHandler = (error) => {
    switch (error.statusCode) {
        case INTERNAL_SERVER_ERROR:
            return {
                type: USERS_ERROR,
                payload: "Something went wrong."
            };
        case BAD_REQUEST:
            return {
                type: USERS_ERROR,
                payload: "User with that account already exists."
            };
        case NOT_FOUND:
            return {
                type: USERS_ERROR,
                payload: "No users were found."
            };
        default:
            return {
                type: USERS_ERROR,
                payload: "Something went wrong."
            };
    }
};
