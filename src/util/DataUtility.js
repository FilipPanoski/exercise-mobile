export const checkIfArrayContains = (array, value) => {
    let itemExists = false;
    array.forEach(item => {
        if (item.id === value.id) {
            itemExists = true;
        }
    });

    return itemExists;
};