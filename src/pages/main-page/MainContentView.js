import React from 'react';
import UserView from "../../reusable/user/UserView";
import {RIGHTS_VIEW, ROLES_VIEW, USERS_VIEW} from "../../reducers/viewReducer";
import RoleView from "../../reusable/role/RoleView";
import RightView from "../../reusable/right/RightView";
import {View} from "react-native";


const MainContentView = (props) => {

    let displayCurrentView = (selectedView) => {
        switch (selectedView) {
            case USERS_VIEW:
                return <UserView/>;
            case ROLES_VIEW:
                return <RoleView/>;
            case RIGHTS_VIEW:
                return <RightView/>;
            default:
                return <UserView/>;
        }
    };

    return (
        <View>
            {displayCurrentView(props.view)}
        </View>
    );
};
export default MainContentView;