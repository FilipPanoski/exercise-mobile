import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from "react-redux";
import { changeView } from "../../actions/viewActions";
import MainNavigation from "../../reusable/navigation/MainNavigation";
import MainContentView from "./MainContentView";

class MainPage extends Component {

    render() {

        return (
            <View style={styles.container}>
                <View>
                    <MainNavigation
                        view={this.props.view}
                        changeView={this.props.onChangeView}
                    />
                </View>

                <View>
                    <MainContentView/>
                </View>
             </View>

         );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = state => ({
    view: state.view.view
});


const mapActionsToProps = {
    onChangeView: changeView
};
export default connect(mapStateToProps, mapActionsToProps) (MainPage);