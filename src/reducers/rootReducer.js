import { combineReducers } from 'redux';
import userReducer from './userReducer';
import viewReducer from "./viewReducer";
import roleReducer from "./roleReducer";
import rightReducer from "./rightReducer";

export default combineReducers({
    users: userReducer,
    roles: roleReducer,
    rights: rightReducer,
    view: viewReducer,
})