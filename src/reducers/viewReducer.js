export const USERS_VIEW = 'view:USERS_VIEW';
export const ROLES_VIEW = 'view:ROLES_VIEW';
export const RIGHTS_VIEW = 'view:RIGHTS_VIEW';
export const CHANGE_VIEW = "view:CHANGE_VIEW";

const initialState = {
    view: USERS_VIEW
};

export default function(state = initialState, action) {
    switch (action.type) {
        /*case CHANGE_VIEW:
            return {
                ...state,
                view: action.payload
            };*/
        default:
            return state;
    }
}