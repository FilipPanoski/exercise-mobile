export const FETCH_RIGHTS = 'rights:FETCH_RIGHTS';
export const RIGHTS_ERROR = 'rights:RIGHTS_ERROR';

const initialState = {
    rights: [],
    right: {},
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        /*case FETCH_RIGHTS:
            return {
                ...state,
                rights: action.payload,
                error: null
            };
        case RIGHTS_ERROR:
            return {
                ...state,
                error: action.payload
            };*/
        default:
            return state;
    }
}