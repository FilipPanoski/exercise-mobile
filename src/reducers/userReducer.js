export const FETCH_USERS = 'users:FETCH_USERS';
export const FETCH_USER = 'users:FETCH_USER';
export const USERS_ERROR = 'users:USERS_ERROR';
export const ADD_USER = 'users:ADD_USER';

const initialState = {
    users: [],
    user: {},
    error: null
};

export default function(state = initialState, action) {
    switch (action.type) {
       /* case FETCH_USERS:
            return {
                ...state,
                users: action.payload,
                user: {},
                error: null
            };
        case FETCH_USER:
            return {
                ...state,
                user: action.payload,
                error: null
            };
        case ADD_USER:
            return {
                ...state,
                user: action.payload,
                error: null
            };
        case USERS_ERROR:
            return {
                ...state,
                error: action.payload
            };*/
        default:
            return state;
    }
}