export const FETCH_ROLES = 'roles:FETCH_ROLES';
export const FETCH_ROLE = 'roles:FETCH_ROLE';
export const FETCH_USER_ROLES = 'roles:FETCH_USER_ROLES';
export const ADD_ROLE_TO_USER = 'roles:ADD_ROLE_TO_USER';
export const REMOVE_RIGHT_FROM_ROLE = 'roles:REMOVE_RIGHT_FROM_ROLE';
export const BAD_ROLE_ERROR = 'roles:ROLE_ERROR';
export const ROLES_ERROR = 'roles:ROLES_ERROR';

const initialState = {
    roles: [],
    userRoles: [],
    role: {},
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        /*case FETCH_ROLES:
            return {
                ...state,
                roles: action.payload,
                role: {},
            };
        case FETCH_USER_ROLES:
            return {
                ...state,
                userRoles: action.payload,
                error: null
            };
        case FETCH_ROLE:
            return {
                ...state,
                role: action.payload,
                roles: [],
                userRoles: [],
                error: null
            };
        case ADD_ROLE_TO_USER:
            return {
                ...state,
                role: action.payload,
                error: null
            };
        case REMOVE_RIGHT_FROM_ROLE:
            return {
                ...state,
                role: action.payload,
                error: null
            };
        case BAD_ROLE_ERROR:
            return {
                ...state,
                error: action.payload
            };
        case ROLES_ERROR:
            return {
                ...state,
                userRoles: [],
                error: action.payload
            };*/
        default:
            return state;
    }
}